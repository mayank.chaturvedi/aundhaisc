# Optimize Optical Parameters for the Aundha interferometer (A1)
There are a number of things in the Advanced LIGO design that we would do differently in hindsight. This repository is aimed at organizing that work.

[Tutorial to gain optical modelling experience](Tutorial.rst)


## Things to optimize
1.  Arm Cavity Finesse
2.  PRC Finesse
3.  SRC Finesse
4.  SRC RoCs - Gouy phase, tolerances, beam size, mode matching tolerances
5.  Sensitivity to point absorbers
6.  ?

### Adjustable parameters
cf <a href="https://dcc.ligo.org/LIGO-T1900587">T1900587</a>

 * RoCs of: ITMs, ETMs, SRM, PRM, SR2, SR3, PR2, PR3 (up to 20-30m change plausible)
 * Reflectivities of: ITMs, ETMs, SRM, PRM, SR2, SR3, PR2, PR3 (finesse changes by factor 2 plausible)
 * Internal cavity lengths (SR2-3, SRM-2, SR3-ITMs), if overall cavity length unchanged
 * ?
 
### Properties to maintain as-is:
 * Sensitivity curve - as for A+ with 6dB sqz
 * control sideband resonances (compatibility with L&H1 electronics systems)
 * beamsplitter properties, including diameter (& resulting clipping) - as for aLIGO
 * ?
 

## Issues to address
### Arm Cavity Finesse
Increase the arm cavity Finesse, will change the DARM linewidth, but not the CARM linewidth (since the T_PRM will be adjusted inversely so as to keep the interferometer impedance matched for the carrier).

Pros:
 * Allows for increasing the SRC Finesse, while keeping the overall DARM pole frequency ~400 Hz.
 * smaller beam size at the BS & ITM

Cons:
 * Increased coating thermal noise from the ITMs (very small effect)
 * harder to lock the single arms during testing

Files: part of DRMI locking below, so see <a href="https://git.ligo.org/IFOsim/aundhaisc/tree/master/DRMi_finesses">DRMi_finesses/</a>

### Arm Cavity g-factor
Make the cavity stable.

Pros:
 * change the Sidles-Sigg effect
 * change the PI situation

Cons:
 * Increased coating thermal noise from the TMs
 * mode-matching to the SRC ?

### DRMi locking (limited by Arm and SRC finesse)
The SRC is tricky to lock because it has a low finesse (mode hopping, …). The choice of SRC finesse resulted from the (somewhat arbitrary, see <a href="https://dcc.ligo.org/LIGO-T070303">T070303</a>) choice of a relatively low finesse for the arm cavities, which was designed to aid lock acquisition but may have been overkill.  

Goal:  Increase the SRM and ITM reflectivities to get better, cleaner signals for SRC and arm locking so that the full DRMi can be locked more reliably and quickly.

Status: ongoing.

Files: <a href="https://git.ligo.org/IFOsim/aundhaisc/tree/master/DRMi_finesses">DRMi_finesses/</a>

### Alignment & Mode Matching Sensing & Control: RC telescopes
Goal: Update the designs of the recycling cavity telescopes (curvatures, internal lengths) so that the accumulated Gouy phase between turning mirrors (P/SR2/3)  is increased, so they can be used as more effective actuators for mode matching (and alignment control), i.e. more orthogonal, optimised in both x and y, lower noise sensing.  

RoCs could also be adjusted to assume the mirrors will be actuated (RHs - or even CO2?), so the cold curvature could be lower than the target value in use (while avoiding the near-unstable virgo situation). 

Status: pending.

Files: tbd.

### Initial lock acquisition (limited by green Arm finesse)
The arm cavities are slow to initially lock because the green finesse is so low. This low finesse also contributes to Arm Length Stabilization noise (ALS diff/error noise), necessitating a CARM offset. Lock acquisition issues took up most of the commissioning time in 2015 and some of 2016, and improved duty cycle can significantly increase the observing time-volume product.

Goal: Increase the ITM green reflectivities to potentially speed this process up. 

Status: pending. 

Files: tbd.

### Others 
Consider also:

 * HOMs (7th order) reaching OMC
 * PIs left after AMDs, PIs introduced due to other changes
 * ?

 
## Files in this directory
 * 
 

## handy links
 * <a href="https://dcc.ligo.org/LIGO-T1900587">T1900587: Use of aLIGO in process spares as India Optics</a>
     * older: <a href="https://dcc.ligo.org/LIGO-L1200053">L1200053: Technical Review Board (TRB) Report: Recommendation regarding which Interferometer (H1 or H2) to Offer to LIGO-India</a>
 * <a href="https://galaxy.ligo.caltech.edu/optics/">Galaxy optics </a>


